<?php

namespace App\Http\Controllers;

use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\dummyModel1;


class Mycontroller extends Controller
{
    public function index()
    {
        echo "hello there - from controller side";
//        return view('Myview');
    }

    public function show()
    {
        $fname = "unknown";
        $age = 15;
//        echo  "Name is - ".$fname." and age is - ".$age;


    }

    public function create()
    {
//        return "Yes, this is it";
//        $dummyModel1 = new dummyModel1;

        $dummyModel1 = dummyModel1::firstOrCreate(['name' => 'Flight 10']);


//        echo $dummyModel1->fill(['name' => 'Flight 22']);

    }

    public function delete(Request $request)
    {

        $dummyModel1 = dummyModel1::find($request->id);

        echo $dummyModel1->delete();
    }


    public function model()
    {

        return $data = dummyModel1::all();
//        return $data = dummyModel1::withTrashed();


        echo "<br><br>";


//        echo count($data) . ' Records Found';

    }

    public function softDelete($sid)
    {
        $dummyModel1 = dummyModel1::find($sid);
        echo $dummyModel1->delete();


    }

    public function update(Request $request)
    {

        $dummyModel1 = dummyModel1::find($request->id);
        $dummyModel1->name = $request->name;
        $dummyModel1->number = $request->number;
        $dummyModel1->address = $request->address;

        echo $dummyModel1->save();
    }

    public function insert(Request $request)
    {

//        print_r($request->input());


//        $dummyModel1 = new dummyModel1(['name'=> 'w']);

//        $dummyModel1 = new dummyModel1;

        $dummyModel1 = dummyModel1::create([
            'name' => $request->name,
            'number' => $request->number,
            'address' => $request->address,
        ]);

        return $dummyModel1;


//        $dummyModel1->name = $request->name;
//        $dummyModel1->number = $request->number;
//        $dummyModel1->address = $request->address;


//        $dummyModel1->name = "Magan";


//        $dummyModel1->save();

//        if ($dummyModel1->save()) {
//            echo "Data inserted " . $dummyModel1;
//        } else {
//            echo "data insertion failed";
//
//        }

    }

}
