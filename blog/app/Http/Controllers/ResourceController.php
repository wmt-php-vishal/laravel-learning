<?php

namespace App\Http\Controllers;

use App\dummyModel1;
use Illuminate\Http\Request;
use App\User;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "welcome to resource controller";
        echo "<br>";
        echo "<a href='res/create'>Insert data</a>";
        echo "<br>";
        echo "<a href='show'>Display data</a>";
        echo "<br>";
//        echo "<a href='edit'>Update data</a>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formRc');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dummymodel1 = new dummymodel1();

        $dummymodel1->name = $request->name;
        $dummymodel1->address = $request->address;
        $dummymodel1->number = $request->number;

        $dummymodel1->save();

        if ($dummymodel1->save()) {
            echo "data inserted successfully";
            return redirect()->action('ResourceController@show');
        } else {
            echo "data insertion failed";
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $dummyModel1 = dummyModel1::all();

        ?>
        <table border="1">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Number</td>
                <td>Address</td>
                <td>Actions</td>
            </tr>

        <?php

        foreach ($dummyModel1 as $dummyModel1) {

            echo "<tr>
                <td> {$dummyModel1->dummy_id} </td>
                <td> {$dummyModel1->name} </td>
                <td> {$dummyModel1->number} </td>
                <td> {$dummyModel1->address} </td>
                <td> <a href='editRc/{$dummyModel1->dummy_id}'>Edit </a> | <a href='deleteRc/{$dummyModel1->dummy_id}'>Delete</a> </td>
            </tr>";
        }


        echo "</table>";
        echo "<a href='resourceController'>Home</a>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($editId)
    {
        $dummyModel1 = dummyModel1::find($editId);
//        echo "Update id is " . $editId;



        return view('updateRc');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {

        $dummyModel1 = dummyModel1::find($request->id);
        $dummyModel1->name = $request->name;
        $dummyModel1->number = $request->number;
        $dummyModel1->address = $request->address;

        echo $dummyModel1->save();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dummyModel1 = dummyModel1::find($id);
//        echo "delete id is -".$id;
        $dummyModel1->delete();
        return redirect()->action('ResourceController@show');
    }
}
