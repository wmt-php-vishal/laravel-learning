<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class dummyModel1 extends Model
{
    use softDeletes;


    protected $fillable = ['name', 'number' , 'address'];
//    protected $guarded = [];
    protected $table = 'dummyModel1';
    protected $primaryKey = 'dummy_id';



}
