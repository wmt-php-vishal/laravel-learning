<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class dummyModel1 extends Model
{
//    use softDeletes;
//Schema::table('dummyModel1', function (Blueprint $table) {
//
//    $table->softDeletes();
//});


//
//$dummyModel1 = App\dummyModel1::all();
//foreach ($dummyModel1 as $dummyModel1) {
//echo $dummyModel1->name;
//}

    protected $fillable = ['name', 'number' , 'address'];
//    protected $guarded = [];
    protected $table = 'dummyModel1';
    protected $primaryKey = 'dummy_id';
//    public $timestamps = false;

}
