<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// redirect to controller
//Route::post('controller','Mycontroller@create');

//Route::match(['get', 'post'], 'controller', 'Mycontroller@create');


//Route::view('zero' , 'Myview' , ['data' => ['name'=> 'Vishal']]);


Route::get('welcome', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');
});

//Route::get('create', 'Mycontroller@create');
//
//Route::view('delete' , 'delete');
//Route::any('deleteid/{id}' ,  'Mycontroller@delete');
//
//
//Route::resource('resourceController', 'ResourceController');
//
//
//Route::any('softDelete/{sid}' ,  'Mycontroller@softDelete');
//
//Route::get('show', 'Mycontroller@model');
//
//Route::get('update', 'Mycontroller@update');
//
//Route::view('update', 'update');
//Route::post('submitupdate', 'Mycontroller@update');
//
//
//Route::view('form', 'form');
//Route::post('submit', 'Mycontroller@insert');
//
//
//Route::permanentRedirect('name', 'user/{name?}');
//
//
//Route::get('/', function () {
//    return view('welcome');
////    return redirect()->route('profile', ['id' => 1, 'photos' => 'yes']);
////    return redirect()->route('profile');
////    echo route('admin.usersonename');die();
//
//
//});
//
//
////Route::get('user/{name?}', function ($name = 'Unknown') {
////
////    echo "Name is - ".$name;
////});
//
//Route::get('user/{name?}/info/{fname}', function ($id, $fname) {
//    return 'User ' . $id . ' => ' . $fname;
//});
//
//Route::get('search/{search}', function ($search) {
//    return $search;
//})->where('search', '.*');
//
////Route::get('user/profile', function () {
////    echo "IFFFF";
////})->name('profile');
//
//Route::get('user/profile', 'Mycontroller@create')->name('profile');
//
//Route::get('user/{id}/profile', function ($id) {
//    echo "IF -sagar";
//    die();
//})->name('profile');
//
//Route::get('admin/profile', function () {
//
//    echo "After";
//})->middleware('CheckAge');
//
//Route::namespace('Home')->group(function () {
//        // Controllers Within The "App\Http\Controllers\Admin" Namespace
//        Route::get('home/baba', 'HomeController@index')->name('home.index');
//});
//
//Route::prefix('admin')->group(function () {
//    Route::get('userone', function () {
//        return 'User one';
//    });
//    Route::get('usertwo', function () {
//        return 'User Two';
//    });
//});
//
//
//Route::name('admin.')->group(function () {
//    Route::get('userone', function () {
//        echo "IF";
//        die();
//
//    })->name('userone');
//
//    Route::get('usertwo', function () {
//        echo "if..else";
//        die();
//
//    })->name('usertwo');
//
//});
//
////Multiple parameters
//
//Route::get('stuName/{stuName?}/{id?}/{marks?}/', function ($stuName = null, $sid = null, $marks = null) {
//    echo "Student Name - " . $stuName . " <br>Id is - " . $sid . "<br> Marks - " . $marks;
//
//})->where(['id' => '[0-9]+'])->where(['stuName' => '[a-zA-Z ]+']);

//Route::middleware('', 'throttle:10000,1')->group(function () {
//
//    Route::get('/', function () {
//
////        echo "<script>alert('rate limit crossed : request blocked ');</script>";
//    });
//});


//Route::fallback(function () {
//    echo "<script>alert('your have entered a unknown route');</script>";
//    echo "This is a fallback function";
//});


//Resource Controller

//loading the insert form
Route::resource('res', 'ResourceController')->names([
    'create' => 'photos.build'
]);




//Route::get('res/create' , 'ResourceController@create');
//
//
////
//////inserting the data
//Route::post('' , 'ResourceController@store');
////
//////display the data
//Route::get('show' , 'ResourceController@show');
////
////
//////update or edit the data
//Route::any('editRc/{editId}' , 'ResourceController@edit');
//Route::any('updateRc' , 'ResourceController@update');
////
////
////
////
//////delete the data
//    //Route::view('deleteRc' , 'delete');
//    Route::any('deleteRc/{dummy_id}' ,  'ResourceController@destroy');

